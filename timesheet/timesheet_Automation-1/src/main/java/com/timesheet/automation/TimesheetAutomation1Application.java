package com.timesheet.automation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TimesheetAutomation1Application {

	public static void main(String[] args) {
		SpringApplication.run(TimesheetAutomation1Application.class, args);
	}

}
